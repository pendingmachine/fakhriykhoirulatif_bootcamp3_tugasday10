import React from "react";

import Avatar from "@mui/material/Avatar";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

import PropTypes from "prop-types";

function MyCard(props) {
  const { title, datePost, img, description } = props;

  return (
    <Card>
      <CardHeader
        avatar={<Avatar sx={{ backgroundColor: "red" }}>R</Avatar>}
        title={title}
        subheader={datePost}
      />
      <CardMedia component="img" image={img} />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
}

MyCard.propTypes = {
  title: PropTypes.string.isRequired,
  datePost: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default MyCard;
