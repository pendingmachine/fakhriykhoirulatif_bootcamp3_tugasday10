import React from "react";

import Grid from "@mui/material/Grid";

import MyCard from "./MyCard";

import { getPost } from "../api/main/post";

import useAsync from "../hooks/useAsync";

function Cards() {
  const { execute, status, value, error } = useAsync(getPost);

  console.log(value);

  return (
    <Grid
      container
      spacing={2}
      sx={{
        border: { xs: "none", sm: "8px solid pink" },
        marginTop: "0",
        paddingRight: "16px",
      }}
    >
      {value ? (
        value.data.map((post, index) => (
          <Grid item xs={12} sm={6} key={index}>
            <MyCard
              title={post.title}
              datePost={post.datePost}
              img={post.img}
              description={post.description}
            />
          </Grid>
        ))
      ) : (
        <p>Loading...</p>
      )}
    </Grid>
  );
}

export default Cards;
