import React from 'react';

import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import ImageIcon from '@mui/icons-material/Image';
import WorkIcon from '@mui/icons-material/Work';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';

function Sidebar() {
  return (
    <List
      sx={{
        width: "100%",
        maxWidth: 360,
        height: "100%",
        bgcolor: "background.paper",
        border: "8px solid peru",
      }}
    >
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="Photos"
          secondary="Jan 9, 2014"
          sx={{ color: "red" }}
        />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <WorkIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="Work"
          secondary="Jan 7, 2014"
          sx={{ color: "red" }}
        />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BeachAccessIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="Vacation"
          secondary="July 20, 2014"
          sx={{ color: "red" }}
        />
      </ListItem>
    </List>
  );
}

export default Sidebar;
