import "./App.css";

import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import Cards from "./component/Cards";
import Navbar from "./component/Navbar";
import Sidebar from "./component/Sidebar";

const theme = createTheme({
  typography: {
    fontFamily: ["Ubuntu", "sans-serif"].join(","),
  },

  palette: {
    primary: {
      main: process.env.REACT_APP_COLOR_PRIMARY
    }
  }
});

function App() {
  return (
    <div>
      <ThemeProvider theme={theme}>
        <Navbar />
        <Container maxWidth="lg">
          <Grid container spacing={4}>
            <Grid item sm={12} md={9}>
              <Cards />
            </Grid>
            <Grid
              item
              sm={0}
              md={3}
              sx={{ display: { sm: "none", md: "block" } }}
            >
              <Sidebar />
            </Grid>
          </Grid>
        </Container>
      </ThemeProvider>
    </div>
  );
}

export default App;
